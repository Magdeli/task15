﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGCharacter.Model.Classes;
using RPGCharacter.Model;
using System.IO;

namespace UItest
{
    public partial class Form1 : Form
    {
        // Magdeli Holmøy Asplin
        // 8/22/2019

        // Attributes that needs declaration
        Character yourCharacter;
        string name;
        string gender;


        
        public Form1()
        {
            InitializeComponent();

            // Creating options in the combobox
            chooseTypeComboBox.Items.Add("Warrior");
            chooseTypeComboBox.Items.Add("Thief");
            chooseTypeComboBox.Items.Add("Mage");

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // Setting the name to be the input from textbox 1
            name = textBox1.Text;
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            // Setting the gender to be the input from textbox 2
            gender = textBox2.Text;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // Creating the character based on which choice the user made from the combobox

            if (chooseTypeComboBox.SelectedItem.ToString() == "Warrior")
            {
                Warrior warrior = new Warrior();
                yourCharacter = new Character(warrior, name, gender);
            }
            else if (chooseTypeComboBox.SelectedItem.Equals("Thief"))
            {
                Thief thief = new Thief();
                yourCharacter = new Character(thief, name, gender);
            }
            else
            {
                Mage mage = new Mage();
                yourCharacter = new Character(mage, name, gender);
            }

            // Printing out a summary of the character to the screen

            MessageBox.Show(yourCharacter.CharacterInfo());

            // Writing the character's type, name and gender to a textfile. This is done such
            // that it can be used in Task15

            using (StreamWriter writetext = new StreamWriter("CharacterInsertSQL.txt"))
            {
                writetext.WriteLine($"INSERT INTO Characters(ClassType, Name, Gender) VALUES(\"{yourCharacter.Class.ClassName}\", \"{name}\", \"{gender}\");");
            }
        }

    }
}
